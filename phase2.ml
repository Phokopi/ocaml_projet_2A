(* CODE DONNE POUR LA PHASE 2 DU PROJET *)

open Analyse;;
(* #use "analyse.ml";; *)










(******************************************************)










(* ALGORITHME DE RESOLUTION DE LA PHASE 2 *)



(* Exception qui sera utilisé quand on cherche une arête dans une liste d'arêtes *)
exception Not_found;;

(* Récupérer le poids de l'arête de src à dst (ou de dst à src) seon une liste d'arêtes disponible.
   val get_arete : 'a -> 'a -> ('a * 'a * 'b) list -> 'a * 'a * 'b = <fun>
   @requires: `node_find1` un noeud.
   @requires: `node_find2` un autre noeud.
   @requires: `toutes_les_aretes` la liste de toutes les arêtes du graphe.
   @returns: l'arête qui relie les 2 noeuds sous forme de triplet : (node_find1, node_find2, weight) où weight est le poids de l'arête.
   @raises: Not_found si l'arête qui relie `node_find1` à `node_find2` (c'est bien l'arête et pas l'arc, donc le résultat sera le même si on inverse l'ordre des arguments) n'est pas trouvée dans la liste `toutes_les_aretes` des arêtes disponibles.
*)
let rec get_arete node_find1 node_find2 toutes_les_aretes =
  match toutes_les_aretes with
  | arete::q -> (* dernière arête *)
    let (n1, n2, weight) = arete in
    if ((node_find1 = n1 && node_find2 = n2) || (node_find1 = n2 && node_find2 = n1))
    then arete
    else get_arete node_find1 node_find2 q
  | _ -> raise Not_found
;;

(* ajoute l'élément `elt` `n` fois à la fin de la liste `l`.
   @requires: `elt` l'élément à ajouter.
   @requires: `n` l'entier qui indique combien de fois on veut ajouter notre élément.
   @requires: `l` la liste à laquelle on veut ajouter notre élément.
   val ajout : 'a -> int -> 'a list -> 'a list = <fun>
*)
let rec ajout elt n l =
  if (n = 0) then l else ajout elt (n-1) (l@[elt])
;;

(* Créer une liste d'arêtes qu'une personne doit parcourir.
   val duplique_chemin_une_personne : 'a list -> ('a * 'a * int) list -> ('a * 'a * int) list = <fun>
   @requires: `chemin` la liste des noeuds que cette personne doit parcourir.
   @requires: `toutes_les_aretes` la liste de toutes les arêtes du graphe.
   @returns: le chemin (= la liste des arêtes) que cette personne doit parcourir, répétées autant de fois (d'affilé) que leur poids.

   Le but est de créer la liste des arêtes à parcourir à partir d'une liste de noeuds à parcourir. Les arêtes sont dupliquées de façon à ce qu'une arête apparaisse autant de fois que son poids.
*)
let duplique_chemin_une_personne chemin toutes_les_aretes =
  let rec aux chemin_aux resultat =
  match chemin_aux with
  | src::dst::[] -> (* dernière arête *)
    (* ajouter l'arete autant de fois que son poids *)
    let arete = get_arete src dst toutes_les_aretes in
    let (_, _, poids) = arete in
    let resultat2 = ajout (src, dst, poids) poids resultat in
    resultat2
  | src::dst::q -> (* pas la dernière arête *)
    (* ajouter l'arete autant de fois que son poids *)
    let arete = get_arete src dst toutes_les_aretes in
    let (_, _, poids) = arete in
    let resultat2 = ajout (src, dst, poids) poids resultat in
    aux (dst::q) resultat2
  | _ -> (* on a fait toute les arêtes, il ne reste qu'un noeud *)
    resultat
  in aux chemin []
;;


(* Créer une liste d'arête que chaque personne doit parcourir.
   val get_chemins_avec_duplications : 'a list list -> ('a * 'a * int) list -> ('a * 'a * int) list list = <fun>
   @requires: `liste_chemins` la liste des chemins de plusieurs personnes.
   @requires: `toutes_les_aretes` la liste de toutes les arêtes du graphe.
   @returns: la liste des chemins (1 chemin = 1 liste d'arêtes) que chaque personne doit parcourir, où les arêtes sont répétées autant de fois (d'affilé) que leur poids.
*)
let get_chemins_avec_duplications liste_chemins toutes_les_aretes =
  List.fold_left (fun acc chemin_une_personne ->
    let chemin_une_personne_duplique = duplique_chemin_une_personne chemin_une_personne toutes_les_aretes in
    acc@[chemin_une_personne_duplique]
  ) [] liste_chemins
;;



(* Des maps dont les clés sont des string (on utilisera cependant la key "1" (string et pas int !) pour représenter la personne numéro 1 pour plus de facilités *)
module MapString = Map.Make(String);;


(* Créer notre map qui nous servira pour notre algorithme de la phase2. Cette map, qui nous servira énormément, a la structure suivante : elle associe le numéro d'une personne (sous forme de string attention !) à un couple de liste. La première liste (appelée "liste de gauche") est la liste des arêtes qu'il reste à parcourir, et la deuxième liste (appelée "liste de droite") est la liste des arêtes déjà parcourue.
   val creer_map : 'a list -> ('a * 'b list) MapString.t = <fun>
   @requires: `chemins_avec_duplications` la liste des chemins (1 chemin = 1 liste d'arêtes) que chaque personne doit parcourir, où les arêtes sont répétées autant de fois (d'affilé) que leur poids.
   @returns: une map dont la structure est décrite plus haut, dans laquelle la liste des arêtes à parcourir (liste de gauche) est strictement égale à la liste des arêtes que cette personne doit parcourir au début de l'algorithme, et où la liste des arêtes à parcourir est = [] (vide).
*)
let creer_map chemins_avec_duplications =
  let (my_map, _)  = List.fold_left(fun (map, entier) chemin_duplique ->
    let map_update = MapString.add (string_of_int entier) (chemin_duplique, []) map in
    (map_update, (entier + 1))
  ) (MapString.empty, 1) chemins_avec_duplications in
  my_map
;;



(* Vérifie si oui ou non une personne a le droit de s'engager sur une certaine arête à un moment donné de l'algorithme.
   val puis_je_mengager : 'a * 'a * 'b -> ('c * (('a * 'a * 'd) list * ('a * 'a * 'd) list)) list -> 'c -> ('a * 'a * 'b) list -> bool = <fun>
   @requires: `arete_normalisee` l'arête sur laquelle la personne veut s'engager, mais normalisée (explication au dessous).
   @requires: `bindings` la liste des couples (key, value) de notre map de l'algorithme. Il s'agit donc d'une liste de couples (personne, (liste gauche, liste droite)) (cf fonction creer_map).
   @requires: `qui_demande` la personne qui demande à s'engager (string, mais c'est un numéro stocké dans un string).
   @requires: `toutes_les_aretes` la liste de toutes les arêtes du graphe.
   @returns: true si personne n'est déjà engagé sur cette arête, true si quelqu'un est déjà engagé sur cette arête mais que cette personne correspond à la personne `qui_demande`, false si quelqu'un d'autre que `qui_demande` est déjà engagé sur l'arête.

   Une arête normalisée est mise sous la forme telle que donnée par les fonctions fournies par le professeur dans le fichier initial. Par exemple, si au début du programme on a stocké l'arête qui relie n1 à n2 avec un poids de 3 sous le triplet ("n1", "n2", 3), alors il faut absolument que cette arête soit avec les noeuds dans cet ordre précis, et non pas ("n2", "n1", 3). *)
let rec puis_je_mengager arete_normalisee bindings qui_demande toutes_les_aretes =
  match bindings with
  | (personne, (l_g, l_d))::q ->
    if (l_g = [] || l_d = [])
    then puis_je_mengager arete_normalisee q qui_demande toutes_les_aretes
    else
      let arete_suivante = List.hd l_g in
      let (src, dst, weight) = arete_suivante in
      let arete_suivante_normalisee = get_arete src dst toutes_les_aretes in
      if (arete_suivante_normalisee = arete_normalisee) && (arete_suivante = List.hd (List.rev l_d))
      (* si je veux utiliser arete_normalisee et que j'en viens déjà, c'est la personne "personne" qui est prioritaire *)
      then personne = qui_demande
      else puis_je_mengager arete_normalisee q qui_demande toutes_les_aretes
  | _ -> true
;;

(* Fait avancer tout le monde d'une étape sur le graphe.
   val bindings_apres_modifs : ('a * ((string * string * int) list * (string * string * int) list)) list -> (string * string * 'b) list -> ('a * ((string * string * int) list * (string * string * int) list)) list = <fun>
   @requires: `bindings_sorted` la liste des couples (key, value) de notre map de l'algorithme triée par ordre décroissant de nombre d'arêtes restantes à parcourir. Il s'agit donc d'une liste de couples (personne, (liste gauche, liste droite)) (cf fonction creer_map).
   @requires: `toutes_les_aretes` la liste de toutes les arêtes du graphe.
   @returns: la liste des bindings (même format qu'en entrée), après qu'on ait essayé de faire avancer tout le monde.

   Si une personne peut avancer, on passe l'arête en tête de sa liste de gauche à la fin de sa liste de droite. Si la personne ne peut pas avancer, on ajoute l'arête ("0", "0", 0) à sa liste de droite, sans toucher sa liste de gauche. Cette arête nulle permet de modéliser le fait qu'une personne n'a pas avancé dans l'espace, mais a avancé dans le temps (a perdu un tour !).

   On rappelle que la liste de gauche contient les arêtes dupliquées autant de fois que leur poids, donc trier par ordre décroissant d'arêtes restantes revient en fait à trier par ordre décroissant de poids total restant ! *)
let bindings_apres_modifs bindings_sorted toutes_les_aretes =
  let (bf, au) = List.fold_left (fun (bindings_final, aretes_utilisees) (personne, (l_g, l_d)) ->
    if (List.length l_g > 0)
    then
      match l_g with
      | arete_todo::l_g_new ->
        let (src, dst, weight) = arete_todo in
        let arete_todo_normalisee = get_arete src dst toutes_les_aretes in
        let je_peux_mengager = puis_je_mengager arete_todo_normalisee bindings_sorted personne toutes_les_aretes in
        if (not (List.mem arete_todo_normalisee aretes_utilisees)) && je_peux_mengager
        then
          let l_d_new = l_d@[arete_todo] in
          let aretes_utilisees_new = arete_todo_normalisee::aretes_utilisees in
          ((personne, (l_g_new, l_d_new))::bindings_final, aretes_utilisees_new) (* peu importe l'ordre car au final on recréera une map à partir de ça *)
        else
          let l_d_new = l_d@[("0", "0", 0)] in
          ((personne, (l_g, l_d_new))::bindings_final, aretes_utilisees)
      | _ -> failwith "pas possible darriver ici"
    else ((personne, (l_g, l_d))::bindings_final, aretes_utilisees) (* la personne a fini son chemin, rien n'est modifié *)
  ) ([], []) bindings_sorted in
  bf
;;

(* Vérifie si l'algorithme est terminé.
   val is_finished_algo : ('a list * 'b) MapString.t -> bool = <fun>
   @requires: `map` une map, qui au départ est créée avec la fonction creer_map, et qui a éventuellement évolué depuis (en faisant avancer des gens). Elle associe à chaque personne sa liste gauche et sa liste droite.
   @returns: true si toutes les personnes ont leur liste gauche vide. false si au moins une personne a encore au moins une arête à parcourir. *)
let is_finished_algo map =
  let nb_elements_listes_gauche = MapString.fold (fun key (l_g,l_d) acc ->
    (List.length l_g) + acc
  ) map 0 in
  nb_elements_listes_gauche = 0
;;

(* Applique mon algorithme qui permet de déterminer le chemin exact de chaque personne (en introduisant des triplets nuls quand la personne ne bouge pas), à partir des chemins de plusieurs personnes et de la liste de toutes les arêtes du graphe.
   val algo : ((string * string * int) list * (string * string * int) list) MapString.t -> (string * string * 'a) list -> ((string * string * int) list * (string * string * int) list) MapString.t = <fun>
   @requires: `map` une map, qui est créée avec la fonction creer_map, et qui n'a pas été modifiée depuis sa création. Elle associe à chaque personne sa liste gauche et sa liste droite.
   @requires: `toutes_les_aretes` la liste de toutes les arêtes du graphe.
   @returns: la map après application de l'algorithme. Pour chaque personne (key de la map), la liste de gauche sera donc vide, et la liste de droite sera le chemin que la personne aura parcouru (avec les doublons qui représentent le poids d'une arête, et éventuellement avec le triplet nul en cas de non avancement à un certain temps). *)
let rec algo map toutes_les_aretes =
  let is_finished = is_finished_algo map in
  if is_finished
  then map
  else
    let bindings_not_sorted = MapString.bindings map in
    let bindings_sorted = List.sort (fun (personne1, (l_g1, l_d1))  (personne2, (l_g2, l_d2)) -> compare (List.length l_g2) (List.length l_g1) (* on trie par ordre décroissant des tailles des listes de gauche *)
    ) bindings_not_sorted in
    let bindings_modified = bindings_apres_modifs bindings_sorted toutes_les_aretes in
    let map_modified = List.fold_left (fun acc (key, value) ->
      let acc_new = MapString.add key value acc in
      acc_new
    ) MapString.empty bindings_modified in
    algo map_modified toutes_les_aretes
;;

(* Obtention du résultat final demandé.
   val get_sol_time : ('a * (string * string * 'b) list) MapString.t -> (string list * int list) list list * int = <fun>
   @requires: `map_apres_algo` la map obtenue après application de l'algorithme dans la fonction `algo`.
   @returns: un couple. Le premier élément est une liste dont les éléments sont les solutions pour chaque personne mises sous la forme demandée par la fonction `output_sol_2`. Le deuxième élément est le temps total mis pour que tout le monde ait finit son trajet.

   Sur un exemple, l'élément pour une personne (sa solution) est une liste, et pourrait être égale à [["a"; "b"; "c"], [3; 4]] (de type (string list * int list) list).
   C'est pour cela que le premier membre du couple renvoyé par cette fonction est de type (string list * int list) list list (une liste de solutions, 1 par personne). *)
let get_sol_time map_apres_algo =
 let (sols, temps_total) = MapString.fold (fun personne (lg, ld) (acc_sols, temps_total_acc) ->
    let solution_personne = List.fold_left(fun acc2 arete ->
      let (src, dst, weight) = arete in
      let (nodes, times, index) = acc2 in

      if (src = "0" && dst = "0") (* arete nulle : on n'a pas avancé *)
      then (nodes, times, (index + 1))
      else if (List.length nodes = 0) (* premier element non nul à ajouter *)
      then ([src; dst], times@[index - 1], (index + 1))
      else if src = (List.hd (List.rev nodes)) (* cas où on a déjà au moins 2 noeuds dans nodes ==> la nouvelle source est égale à l'ancienne destination (exemple n1n2, n2n4), on ajoute que n4 *)
      then (nodes@[dst], times@[index - 1], (index + 1))
      else (* src <> (List.hd (List.rev nodes)) i.e. on répète une arête pour dupliquer le poids, donc osef on l'ajoute pas *)
        (nodes, times, (index + 1))


      ) ([], [], 1) ld in
    let (nodes, times, compteur) = solution_personne in
    let temps_total_maj = max temps_total_acc (List.length ld) in
    (acc_sols@[[nodes, times]], temps_total_maj)
  ) map_apres_algo ([], 0) in
(sols, temps_total)
;;










(******************************************************)










(* Application de l'algorithme à notre jeu de test *)




(* fichier à traiter donné en 1er argument à l'execution.
   `file` est un string.*)
let file = Sys.argv.(1);;
(* let file = "2_2.txt";; *)

(* récupère les données du fichier file. links_triplet est la liste des arêtes disponibles, chemins est la liste qui contient tous les chemins (le premier element est le chemin de la premiere personne, etc ...)
   val links_triplet : (string * string * int) list
   val chemins : string list list
*)
let links_triplet, chemins = analyse_file_2 file;;

(* par exemple, on a pour le fichier 2.txt les éléments suivants :
   val links_triplet :
   (string * string * int) list = [("n2", "n4", 3); ("n1", "n2", 1)]
   val chemins :
   string list list = [["n1"; "n2"; "n4"]; ["n4"; "n2"]]
*)

(* on récupère tous les chemins avec duplications selon les poids *)
let chemins_dupliques = get_chemins_avec_duplications chemins links_triplet;;

(* on crée la map associée *)
let my_map = creer_map chemins_dupliques;;

(* test d'affichage de la map *)
let _ = MapString.fold (fun key value acc ->
  (key,value)::acc
) my_map [];; (*affichage de notre map*)

(* application de l'algorithme pour avoir la map finale, avec les triplets nuls dans la liste de droite *)
let my_map_apres_algo = algo my_map links_triplet;;

(* test d'affichage de la map après algo *)
let _ = MapString.fold (fun key value acc ->
  (key,value)::acc
) my_map_apres_algo [];; (*affichage de notre map*)

(* on recupère les chemins de chacun et le temps total *)
let list_sol, temps_total = get_sol_time my_map_apres_algo;;

(* test d'affichage des solutions *)
let _ = List.fold_left (fun acc elt -> acc@[elt]) [] list_sol;;

List.iter (fun elt -> output_sol_2 elt) list_sol;;
Format.printf "%d" temps_total;;
