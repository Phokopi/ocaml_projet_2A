(* QUELQUES MODULES POUR NOS TYPES *)

(* Les clés de notre association *)
module OrderedString =
struct
  type t = string
  let compare = compare
end
;;


(* Les éléments des ensembles dans l'association *)
module OrderedCoupleStringInt =
struct
  type t = string * int
  let compare = compare
end
;;





(* NOTRE MODULE DE GRAPHE *)


(* Définition de notre type Set et de notre type Map *)
module MySet = Set.Make(OrderedCoupleStringInt);;
module MyMap = Map.Make(OrderedString);;

(* Notre graphe va associer des clés à des ensembles. Les clés sont les clés de notre map, et les ensembles sont notre type set *)
type graph = MySet.t MyMap.t;;
type set = MySet.t;;

(* Le graphe vide est la map vide *)
let empty = MyMap.empty;;

(* Test de vacuité du graphe `g` *)
let is_empty g = MyMap.is_empty g
;;

(* Renvoie l'ensemble associé à la clé `src` dans le graphe `g` *)
let succs src g = MyMap.find src g
;;

(* renvoie les noeuds reliés par une arête à src sous forme d'une liste de string *)
let succs_nodes_list src g =
  let succs_set = succs src g in
  MySet.fold (fun elt acc ->
    let (node, weight) = elt in
    node::acc
  ) succs_set []
;;

(* Ajout d'une liaison de `src` vers l'élément `(dst, weight)` dans le graphe `g` :
1) On récupère l'ancien set associé à la clé src (ou à défaut, un ensemble vide)
2) On ajoute à cet ensemble l'élément (dst, weight)
3) On ajoute au graphe la liaison src -> [nouvel ensemble après ajout]
4) On renvoie le graphe obtenu *)
let add_one_edge src dst weight g =
  let old_set =
    try MyMap.find src g
    with Not_found -> MySet.empty in
  let new_set = MySet.add (dst, weight) old_set in
  let new_g = MyMap.add src new_set g in
  new_g
;;

(* ajoute les 2 arêtes : de src à dst, et de dst à src *)
let add_both_edges src dst weight g =
  add_one_edge dst src weight (add_one_edge src dst weight g)
;;

(* Suppression d'une liaison de `src` vers l'élément `dst` dans le graphe `g` :
1) On récupère l'ancien set associé à la clé src (ou à défaut, un ensemble vide)
2) On retire à cet ensemble les éléments dont le string du couple est dst
3) On ajoute au graphe la liaison src -> [nouvel ensemble après retrait]
4) On renvoie le graphe obtenu
Dans le cas où src n'est pas associé à un ensemble, on renvoie g *)
let remove_one_edge src dst g =
  try
    let old_set = succs src g in
    let new_set = MySet.filter (fun (clef, poids) -> if clef = dst then false else true) old_set in
    let new_g = MyMap.add src new_set g in
    new_g
  with Not_found -> g
;;

(* enlève les 2 arêtes : de src à dst, et de dst à src. *)
let remove_both_edges src dst g =
  let g_without_src_to_dst = remove_one_edge src dst g in
  remove_one_edge dst src g_without_src_to_dst
;;

(* Fold la fonction `f` au graphe `g` avec la valeur de départ `v0`
   Dans cette fonction, le fold a lieu sur les noeuds : on applique la fonction f aux clés de l'association du graphe *)
let fold_node f g v0 =
  MyMap.fold (fun n set acc ->
    f n acc) g v0
;;

(* Fold la fonction `f` au graphe `g` avec la valeur de départ `v0`
   Dans cette fonction, le fold a lieu sur les arêtes : pour chaque clé de l'association, on applique un fold sur l'ensemble associé à cette clé, sans toucher au poids *)
let fold_edge f g v0 =
  MyMap.fold (fun src set acc ->
    MySet.fold (fun (dst, weight)  acc_set ->
      f src dst weight acc_set) set acc
  ) g v0
;;

(* Renvoie les noeuds du graph sous forme de liste *)
let nodes_to_list g =
  let nodes_non_tries = fold_node (fun src acc ->
    src::acc
  ) g [] in
  List.sort (compare) nodes_non_tries
;;

(* Renvoie les arêtes du graphes sous forme de liste triée *)
let edges_to_list g =
  let liste_non_triee = fold_edge (fun src dst weight acc ->
    (src^" --> "^dst^" (duree : "^string_of_int weight^")")::acc
  ) g [] in
  List.sort (compare) liste_non_triee
;;

(* on parcourt toute la liste de liaisons, et on ajoute à un graph accumulateur.
   Quand on arrive à la fin, on renvoie le graphe accumulé *)
let rec create_graph_aux links g_acc = match links with
  | [] -> g_acc
  | (s1, s2, weight)::t ->
    let new_g = add_both_edges s1 s2 weight g_acc in
    create_graph_aux t new_g
;;

(* Crée un graphe pondéré orienté réciproque à partir d'une liste de liaisons de la forme (string * string * int) list, comme fournies par la fonction analyse_file_1 *)
let create_graph links = create_graph_aux links empty
;;

(* Retourne le poids entre 2 arêtes dans le graphe. S'il n'existe pas d'arête de s1 à s2, renvoie -1 *)
let find_weight s1 s2 g = fold_edge (fun src dst weight acc ->
  if (s1 = src && s2 = dst) then weight else acc
) g (-1)
;;
