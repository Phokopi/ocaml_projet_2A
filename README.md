# Projet OCaml 2A (2017-2018)

Par Anthony Barbier

## Quels fichiers dans cette archive

> Les fichiers `analyse.ml` et `analyse.mli` sont fournis.

> - Le fichier `myGraph.ml` permet d'utiliser des graphes, qui relient (via un *Map*) un ***string*** à un ensemble (*Set*) dont les éléments sont des couples ***(string, int)***.
> - Une interface détaillée est fournie dans le fichier `myGraph.mli`.

> Les fichiers `phase1.ml`, `phase2.ml` et `phase3.ml` sont les fichier qui contiennent ce qui est demandé pour ces phases dans l'énoncé.

## Compilation

Un Makefile est disponible. Pour compiler toutes les phases d'un coup, lancer simplement la commande `make` (ou bien `make all`) dans la console.

Pour compiler une phase en particulier, lancer `make phase1` pour la phase 1, `make phase2` pour la phase 2, ou `make phase3` pour la phase 3.

En cas de problème avec l'utilisation du Makefile (si on est sur Windows par exemple lol), on peut compiler chacune des phases en lançant ces lignes de commande :

- phase 1 : `ocamlc -c analyse.mli && ocamlc -c analyse.ml && ocamlc -c myGraph.mli && ocamlc -c myGraph.ml && ocamlc -c phase1.ml && ocamlc -o phase1.out analyse.cmo myGraph.cmo phase1.cmo`
- phase 2 : `ocamlc -c analyse.mli && ocamlc -c analyse.ml && ocamlc -c phase2.ml && ocamlc -o phase2.out analyse.cmo myGraph.cmo phase2.cmo`
- phase 3 : `ocamlc -c analyse.mli && ocamlc -c analyse.ml && ocamlc -c myGraph.mli && ocamlc -c myGraph.ml && ocamlc -c phase3.ml && ocamlc -o phase3.out analyse.cmo myGraph.cmo phase3.cmo`

## Lancer les tests

Une fois compilé, il suffit de lancer le fichier `phase1.out` avec comme premier argument le fichier à analyser.

Par exemple, si on souhaite essayer la phase 1 avec le fichier "1.txt", il faut lancer la commande `./phase1.out 1.txt`.
Idem pour les autres phases donc.

Le résultat voulu est ensuite affiché dans la console, dans le format demandé par l'énonce.
