(* CODE DONNE POUR LA PHASE 1 DU PROJET *)

open Analyse;;
(* #use "analyse.ml";; *)










(******************************************************)










(* MA STRUCTURE DE GRAPHE PONDERE ORIENTE *)

open MyGraph;;
(* #use "myGraph.ml" *)









(******************************************************)










(* MON ALGORITHME DIJKSTRA A PARTIR DE WIKIPEDIA *)

(* Des maps dont les clés sont des strings. A un noeud, on associera son poids dans l'algorithme de dijkstra (c'est bien le poids du noeud et non pas le poids d'une quelconque arête). *)
module MapString = Map.Make(String);;

(* Le poids d'un noeud est soit un entier, soit Infini *)
type weight_node = Infinity | Weight of int;;

(* Initialisation pour l'algorithme de Dijkstra.
   val dijkstra_initialisation : 'a MyMap.t -> MyMap.key -> (weight_node * MyMap.key list) MapString.t = <fun>
   @requires: `g` un graphe obtenu grâce au module MyGraph.
   @requires: `sdeb` le noeud de départ (un string).
   @returns: la map qui à un noeud associe son poids au début de l'algorithme de Dijkstra et un chemin vide (sauf pour `sdeb`).

   Le poids du noeud de départ est 0, le poids de tous les autres noeuds est Infinity. *)
let dijkstra_initialisation g sdeb =
  let map_poids = fold_node (fun node acc ->
    let couple_poids_chemin =
      if (node = sdeb) then (Weight(0), [sdeb])
      else (Infinity, []) in
    MapString.add node couple_poids_chemin acc
  ) g MapString.empty in
  map_poids
;;

(* Trouver le noeud de poids minimum parmi une liste de noeuds.
   val trouve_min : MapString.key list -> (weight_node * 'a) MapString.t -> MapString.key = <fun>
   @requires: `q` la liste des noeuds dont on veut trouver celui qui a le poids le plus petit.
   @requires: `map_poids` la map qui à chaque noeud associe son poids dans l'algorithme de Dijkstra.
   @raises: `Not_found` si un des éléments de la liste `q` n'est pas dans la map `map_poids`.
   @returns: Le sommet de `q` dont le poids est minimal dans `map_poids`. *)
let trouve_min q map_poids =
  let couple_poids_sommet = List.fold_left(fun acc s ->
    let (mini, sommet_poids_mini) = acc in
    let (poids_s, _) = MapString.find s map_poids in
    match mini, poids_s with
    | Infinity, Infinity -> (* le sommet n'est pas meilleur *)
      acc
    | Weight(_), Infinity -> (* pareil, pas mieux *)
      acc
    | Infinity, Weight(_) -> (* on a trouvé meilleur *)
      (poids_s, s)
    | Weight(w_mini), Weight(w_s) -> (* on a peut-être trouvé meilleur *)
      if (w_s < w_mini) (* si on a trouvé mieux *)
      then (poids_s, s) (* on met à jour le minimum *)
      else acc (* sinon on garde le même minimum *)
  ) (Infinity, "") q in
  let (poids, sommet) = couple_poids_sommet in
  sommet
;;


(* Mise à jour des poids dans l'algorithme de Dijkstra.
   val maj_distances : MapString.key -> MapString.key -> (weight_node * MapString.key list) MapString.t -> MySet.t MyMap.t -> (weight_node * MapString.key list) MapString.t = <fun>
   @requires: `s1` un sommet de notre graphe (string).
   @requires: `s2` un autre sommet de notre graphe (string).
   @requires: `map_poids` la map qui à chaque noeud associe son poids dans l'algorithme de Dijkstra.
   @requires: `g` un graphe obtenu grâce au module MyGraph.
   @returns: La nouvelle map des poids, après mise à jour du poids de s2.
   @raises: `Not_found` si `s1` et/ou `s2` n'est pas dans la map `map_poids`.

   Dans cette fonction on met à jour le poids de s2 dans la map des poids. Le nouveau poids de s2 est égal au minimum entre l'ancien poids de s2, et le poids de s1 + le poids de l'arête pour aller de s1 à s2. Entre autres, on regarde ici s'il vaut mieux d'abord passer par s1 pour aller à s2 plutôt que d'aller en s2 directement, et on met à jour le poids pour aller en s2 en conséquence. *)
let maj_distances s1 s2 map_poids g =
  let (poids_s1, chemin_s1) = MapString.find s1 map_poids in (* un type Weight *)
  let (poids_s2, chemin_s2) = MapString.find s2 map_poids in (* un type Weight *)
  let poids_edge_s1_s2 = find_weight s1 s2 g in (* un int *)
  match poids_s1, poids_s2 with
  | Infinity, Infinity -> (* on ne fait pas mieux *)
    map_poids
  | Weight(w), Infinity -> (* alors d[s2] > d[s1] + Poids(s1,s2) car Poids(s1,s2) n'est jamais infini *)
    MapString.add s2 (Weight(w + poids_edge_s1_s2), s2::chemin_s1) map_poids (* on met à jour le poids de s2 : il vaut mieux d'abord passer par s1 *)
  | Infinity, Weight(_) -> (* s1 a un poids infini, il vaut mieux ne pas passer par s1 mais aller directement en s2 comme c'était le cas *)
    map_poids
  | Weight(w_s1), Weight(w_s2) -> (* on trouve mieux ssi d[s2] > d[s1] + Poids(s1,s2) *)
    if (w_s2 > w_s1 + poids_edge_s1_s2) (* on a trouvé mieux *)
    then MapString.add s2 (Weight(w_s1 + poids_edge_s1_s2), s2::chemin_s1) map_poids
    else (* pas trouvé mieux *)
      map_poids
;;


(* Fonction principale de l'algorithme de Dijkstra
   val dijkstra : MySet.t MyMap.t -> MyMap.key -> (weight_node * MapString.key list) MapString.t = <fun>
   @requires: `g` un graphe obtenu grâce au module MyGraph (qui modélise les arêtes).
   @requires: `sdeb` le noeud de départ (un string).
   @returns: la map qui à chaque noeud du graphe `g` associe un couple (temps mis pour aller de `sdeb` à ce noeud, chemin à suivre).

   Cette fonction calcule grâce à l'algorithme de Dijkstra le temps total et le chemin optimal pour aller à chaque noeud, en partant de `sdeb`.
*)
let dijkstra g sdeb =
  (* intitialisation *)
  let map_poids = dijkstra_initialisation g sdeb in
  (* définition de Q *)
  let q = nodes_to_list g in
  (* boucle principale tant que Q n'est pas vide *)
  let rec while_aux q_aux map_poids_aux =
    match q_aux with
    | [] -> (* fin de la boucle si q_aux est vide *)
      map_poids_aux
    | _ -> (* on continue la boucle *)
      (* récupère le minimum s1, et met à jour q_aux *)
      let s1 = trouve_min q_aux map_poids_aux in
      let q_aux_without_s1 = List.filter (fun elt -> elt <> s1) q_aux in
      (* pour chaque noeud s2 voisin de s1, mettre à jour la distance de sdeb à s2 *)
      let voisins_s1 = succs_nodes_list s1 g in
      let map_poids_aux_maj = List.fold_left (fun acc s2 ->
        maj_distances s1 s2 acc g
      ) map_poids_aux voisins_s1 in
      while_aux q_aux_without_s1 map_poids_aux_maj
  in
  let new_map_poids = while_aux q map_poids in
  new_map_poids
;;










(******************************************************)










(* AFFICHAGE DE LA SOLUTION A LA PHASE 1 *)



(* fichier à traiter donné en 1er argument à l'execution.
   `file` est un string.*)
let file = Sys.argv.(1);;
(* let file = "1.txt";; *)

(* récupère les données du fichier file. links1 est la liste des arêtes disponibles, start1 le noeud de départ, et end1 le noeud d'arrivée.
   val links1 : (string * string * int) list
   val start1 : string
   val end1 : string
*)
let links1, (start1, end1) = analyse_file_1 file;;

(* création du graphe à partir de la liste d'arêtes fournie dans le file.
   val graph1 : MySet.t MyMap.t = <abstr>
*)
let graph1 = create_graph links1;;

(* liste des noeuds et des arêtes du graphe crée ci-dessus
   val list_nodes : MyMap.key list (string list en fait)
   val list_edges : string list
*)
let list_nodes = nodes_to_list graph1;;
let list_edges = edges_to_list graph1;;


(* affichage des données de départ. À commenter avant de rendre le projet, car il est demandé pour unique affichage l'ouput de la solution *)
(* print_endline "";;
   print_endline "Phase 1 : ";;
   print_endline "";;
   print_endline ("# Noeuds du graphe : "^(List.fold_right (fun elt acc -> elt^" "^acc) list_nodes ""));;
   print_endline "";;
   print_endline ("# Arcs du graphe : ");;
   List.iter (Format.printf "  - %s\n") (list_edges);;
   print_endline "";;
   print_endline "# Resolution : ";;
   print_endline "";;
   print_endline ("Depart : "^start1);;
   print_endline ("Arrivee : "^end1);;
   print_endline "";;
   print_endline ("La solution optimale est : ");;
*)


(* Application de l'algorithme pour trouver le temps minimum et le chemin associé *)

(* la map qui à chaque noeud (string) de `graph1` associe le couple (temps total pour venir à ce noeud depuis `start1`, chemin optimal depuis `s1` *)
let map_poids_et_chemins = dijkstra graph1 start1;;

(* on récupèré le couple de la map ci-dessus pour le noeud end1. On a alors le temps minimal ainsi que le chemin optimal pour aller de start1 à end1. Dans le cas où l'algorithme de Dijkstra n'aurait pas fonctionné, on donne un poids de -1, afin d'avoir quand même un int pour notre affichage et bien savoir qu'on a une erreur. *)
let (time_end1, chemin_end1) = match MapString.find end1 map_poids_et_chemins with
| Infinity, chemin -> (-1), List.rev chemin
| Weight(w), chemin -> w, List.rev chemin
;;


(* AFFICHAGE DU RESULTAT *)

output_sol_1 time_end1 chemin_end1
;;
