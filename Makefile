all: phase1 phase2 phase3

phase1: analyse.cmo myGraph.cmo phase1.cmo
	ocamlc -o phase1.out analyse.cmo myGraph.cmo phase1.cmo

phase1.cmo: phase1.ml
	ocamlc -c phase1.ml

phase2: analyse.cmo phase2.cmo
	ocamlc -o phase2.out analyse.cmo phase2.cmo

phase2.cmo: phase2.ml
	ocamlc -c phase2.ml

phase3: analyse.cmo myGraph.cmo phase3.cmo
	ocamlc -o phase3.out analyse.cmo myGraph.cmo phase3.cmo

phase3.cmo: phase3.ml
	ocamlc -c phase3.ml

analyse.cmo: analyse.cmi analyse.ml
	ocamlc -c analyse.ml

analyse.cmi: analyse.mli
	ocamlc -c analyse.mli

myGraph.cmo: myGraph.cmi myGraph.ml
	ocamlc -c myGraph.ml

myGraph.cmi: myGraph.mli
	ocamlc -c myGraph.mli
