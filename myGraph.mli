(* Notre type de graphe, qui associe des string à des set (ensembles) *)
type graph

(* Le type d'arrivée de notre graphe : un ensemble dont les éléments sont des couples (string, int) *)
type set

(* Le graphe vide *)
val empty : graph

(* Test de vacuité d'un graphe.
`is_empty g` renvoie vrai si le graphe est vide, faux sinon *)
val is_empty : graph -> bool

(* Renvoie l'ensemble associé à une clé dans un graphe.
`succs src g` renvoie l'ensemble associé à la clé `src` dans le graphe `g` *)
val succs : string -> graph -> set

(* Renvoie la liste des noeuds reliés à un autre noeud.
`succs_nodes_list src g` renvoie la liste des noeuds reliés à `src` par une arête dans le graphe `g`*)
val succs_nodes_list : string -> graph -> string list

(* Ajout d'une arête dans un graphe (attention c'est bien une arête, ce n'est pas un arc !)
`add_both_edges src dst weight g` ajoute l'arête qui relie `src` et `dst` avec le poids `weight` dans le grpahe `g` *)
val add_both_edges : string -> string -> int -> graph -> graph

(* Retrait d'une arête dans un graphe
`remove_one_edge src dst g` retire l'arête qui relie les noeuds `src` et `dst` dans le graphe `g` *)
val remove_both_edges : string -> string -> graph -> graph

(* Fold d'un graphe sur les noeuds (clés)
`fold_node f g v0` fold la fonction `f` aux noeuds du graphe `g` avec une valeur de départ de `v0` *)
val fold_node : (string -> 'a -> 'a) -> graph -> 'a -> 'a

(* Fold d'un graphe sur les arêtes (noeud de départ, noeud d'arrivée, poids)
`fold_edge f g v0` fold la fonction `f` à chaque arête = triplet (noeud de départ, noeud d'arrivée, poids) avec une valeur de départ de v0 *)
val fold_edge : (string -> string -> int -> 'a -> 'a) -> graph -> 'a -> 'a

(* Création d'une liste des noeuds du graphe
`nodes_to_list g` renvoie la liste des noeuds (clés) du graphe `g` *)
val nodes_to_list : graph -> string list

(* Création d'une liste des arêtes du graphe
`edges_to_list g` renvoie la liste des arêtes du graphe `g`. Si une arête relie [src] et [dst] avec le poids [weight], la string associée dans la liste renvoyée sera exactement : "src --> dst (duree : weight)" *)
val edges_to_list : graph -> string list

(* Création d'un graphe à partir d'une liste d'arêtes
`create_graph links` renvoie le graph correspondant à la liste d'arêtes `links` *)
val create_graph : (string * string * int) list -> graph

(* Trouver le poids d'une arête qui relie 2 noeuds
`find_weight s1 s2 g` renvoie le poids de l'arête qui relie `s1` et `s2` dans le graphe `g`. S'il n'existe pas d'arête de s1 à s2, renvoie -1 *)
val find_weight : string -> string -> graph -> int
